package by.epam.references.tests;

import by.epam.references.entity.TestObject;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class SoftReferenceTest
{
    public static void main(String[] args) {
        List<Reference<TestObject>> softList = new ArrayList<Reference<TestObject>>();
        for (int i = 0; i < 3; i++) {
            softList.add(new SoftReference<TestObject>(new TestObject(i)));
            System.out.println(softList.get(i).get());
        }
        List<String> loadMemoryList = new ArrayList<>();

        try {
            for (int i = 0; i < 100000000; i++) {
                loadMemoryList.add(i + "");
            }
        } catch (OutOfMemoryError e) {

        }
        System.gc();
        for (Reference<TestObject> ref : softList)
            System.out.print(ref.get() + " ");

    }
}
