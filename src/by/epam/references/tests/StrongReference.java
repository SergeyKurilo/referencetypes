package by.epam.references.tests;

import by.epam.references.entity.TestObject;

public class StrongReference {
    public static void main(String[] args) {
        TestObject testObject = new TestObject(1);
        System.gc();
        System.out.println(testObject);
        testObject = null;
        System.gc();
        System.out.println(testObject);
    }
}
