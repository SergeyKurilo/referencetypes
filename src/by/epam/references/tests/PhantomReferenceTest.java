package by.epam.references.tests;

import by.epam.references.entity.TestObject;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.List;

public class PhantomReferenceTest {
    public static void main(String[] args) throws InterruptedException {
        List<Reference<TestObject>> phantomList = new ArrayList<Reference<TestObject>>();
        ReferenceQueue<TestObject> queue =  queue = new ReferenceQueue<TestObject>();
        for (int i = 0; i < 3; i++) {
            phantomList.add(new PhantomReference<TestObject>(new TestObject(i), queue));
            System.out.println(phantomList.get(i).get());
        }
        System.gc();
        System.out.println("garbage collector invoked");
        for (Reference<TestObject> ref : phantomList)
            System.out.print(ref.get() + " ");

    }
}
