package by.epam.references.tests;

import by.epam.references.entity.TestObject;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class WeakReferenceTest {
    public static void main(String[] args) {
        List<Reference<TestObject>> weakList = new ArrayList<Reference<TestObject>>();
        for (int i=0; i<3; i++){
            weakList.add(new WeakReference<TestObject>(new TestObject(i)));
            System.out.println(weakList.get(i).get());
        }
        System.gc();
        System.out.println("garbage collector invoked");
        printWeakList(weakList);

    }

    private static void printWeakList(List<Reference<TestObject>> weakList) {
        for (Reference<TestObject> ref : weakList)
            System.out.print(ref.get() + " ");
    }
}
