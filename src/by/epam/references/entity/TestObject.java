package by.epam.references.entity;

public class TestObject {
    private int k;

    public TestObject(int k) {
        this.k = k;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("TestObject: " + k + " finalize");
    }
}
